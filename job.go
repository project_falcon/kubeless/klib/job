package job

const (
	InitContainers = "init"
	RunContainers  = "run"
)

//YamlRoot root of Job struct
type YamlRoot struct {
	Fc2 *BuildType `yaml:"fc2,flow"`
}

// BuildType describes 3 types of job
type BuildType struct {
	Test  *BuildConfig `yaml:"test,flow"`
	Build *BuildConfig `yaml:"build,flow"`
	Kbox  *BuildConfig `yaml:"kbox,flow"`
}

// BuildConfig describe build
type BuildConfig struct {
	Init    map[int]Container `yaml:"init,flow"`
	Run     map[int]Container `yaml:"run,flow"`
	Vol     *Volumen           `yaml:"vol,flow"`
	Default *Default           `yaml:"default,flow"`
}

// Container describe definitions of container
type Container struct {
	Name       string            `yaml:"name"`
	Image      string            `yaml:"img"`
	WorkingDir string            `yaml:"workdir"`
	Commands   []string          `yaml:"cmds"`
	Args       []string          `yaml:"args"`
	Vol        map[string]string `yaml:"vol"`
	Ports      map[string]int32  `yaml:"ports"`
	Memory     *Memory            `yaml:"mem"`
	CPU        *CPU               `yaml:"cpu"`
}

//Volumen describe 3 types of volumen
type Volumen struct {
	EmptyDir  []string                    `yaml:"emptydir"`
	ConfigMap map[string]VolumenConfigMap `yaml:"configmap,flow"`
	Secret    map[string]VolumenSecret    `yaml:"secret,flow"`
}

//VolumenConfigMap  describe configmap
type VolumenConfigMap struct {
	Key  string `yaml:"key"`
	Path string `yaml:"path"`
}

//VolumenSecret describe secret
type VolumenSecret struct {
	Name string `yaml:"name"`
}

type Default struct {
	Memory *Memory `yaml:"mem"`
	CPU    *CPU    `yaml:"cpu"`
}

type Memory struct {
	Limit   string `yaml:"limit"`
	Request string `yaml:"request"`
}

type CPU struct {
	Limit   string `yaml:"limit"`
	Request string `yaml:"request"`
}

func (j *YamlRoot) IfKboxEmpty() bool {
	return j.Fc2.Kbox == nil
}

func (j *YamlRoot) IfBuildEmpty() bool {
	return j.Fc2.Build == nil
}

func (j *YamlRoot) IfTestEmpty() bool {
	return j.Fc2.Test == nil
}

func (d *Default) VerifyStruct() *Default{
	if d == nil {
		return new(Default)
	}
	
	return d
}

func (m *Memory) VerifyStruct() *Memory{
	if m == nil {
		return new(Memory)
	}
	
	return m
} 

func (c *CPU) VerifyStruct() *CPU{
	if c == nil {
		return new(CPU)
	}
	
	return c
} 

func (b *BuildConfig) VerifyStruct() *BuildConfig{
	if b == nil {
		return new(BuildConfig)
	}
	
	return b
} 

func (v *Volumen) VerifyStruct() *Volumen{
	if v == nil {
		return new(Volumen)
	}
	
	return v
}

func (c *Container) VerifyStruct() *Container{
	if c == nil {
		return new(Container)
	}
	
	return c
} 

func (b *BuildType) VerifyStruct() *BuildType{
	if b == nil {
		return new(BuildType)
	}
	
	return b
} 

